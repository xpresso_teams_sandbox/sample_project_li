import os
import sys
import types
import pickle
import marshal
import numpy as np
import pandas as pd
import seaborn as sns
import xgboost as xgb
import matplotlib.pyplot as plt
from sklearn.metrics import brier_score_loss
from imblearn.over_sampling import SMOTE,SVMSMOTE
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix,classification_report,accuracy_score
from sklearn.metrics import plot_precision_recall_curve,precision_recall_curve,average_precision_score
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    X_train = pickle.load(open(f"{PICKLE_PATH}/X_train.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    y_train = pickle.load(open(f"{PICKLE_PATH}/y_train.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    clean_data = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/clean_data.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    modeling = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/modeling.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))

# Smote transformation, take train and test data, output is SMOTE data
## $xpr_param_component_name == smote_transform
## $xpr_param_component_type == pipeline_job
## $xpr_param_global_variables == ["X_train", "y_train"]
## $xpr_param_global_methods == ["clean_data", "modeling"]

def smote_transform(ratio):
    sm = SMOTE(random_state=42,sampling_strategy=ratio)
    X_smote, y_smote = sm.fit_resample(X_train,y_train)
    return X_smote,y_smote

# need to save all transformed data into desk, from location to read training.
# create one more component: create a new dataset after SMOTE
# ratios=list(np.arange(0.15,0.3,0.05))
# for ratio in ratios:
#     print(ratio)
#     names= str(round(ratio,3))+' smote random forest'
#     sm = SMOTE(random_state=42,sampling_strategy=ratio)
#     X_smote, y_smote = sm.fit_resample(X_train,y_train)
#     fit_predict_score(X_smote,y_smote,name=names,clf=RandomForestClassifier(max_depth=10, random_state=42,
#                                                           max_features="sqrt",criterion="entropy",
#                                                                         class_weight="balanced"))
#     print('\n')
    
sm = SMOTE(random_state=42,sampling_strategy=0.3)
X_smote, y_smote = sm.fit_resample(X_train,y_train)
fit_predict_score(X_smote,y_smote,name='0.3 smote random forest',clf=RandomForestClassifier(max_depth=10, random_state=42,
                                                          max_features="sqrt",criterion="entropy",
                                                                        class_weight="balanced"))
ratios=list(np.arange(0.15,0.35,0.05))
for ratio in ratios:
    print(ratio)
    names= str(round(ratio,3))+' smote XGB'
    sm = SMOTE(random_state=42,sampling_strategy=ratio)
    X_smote, y_smote = sm.fit_resample(X_train,y_train)
    xgb_function(X_smote,y_smote,names)
    print('\n')

try:
    pickle.dump(marshal.dumps(clean_data.__code__), open(f"{PICKLE_PATH}/clean_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(modeling.__code__), open(f"{PICKLE_PATH}/modeling.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    pickle.dump(X_train, open(f"{PICKLE_PATH}/X_train.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(y_train, open(f"{PICKLE_PATH}/y_train.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

