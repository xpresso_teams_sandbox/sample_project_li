import os
import sys
import types
import pickle
import marshal
import numpy as np
import pandas as pd
import seaborn as sns
import xgboost as xgb
import matplotlib.pyplot as plt
from sklearn.metrics import brier_score_loss
from imblearn.over_sampling import SMOTE,SVMSMOTE
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix,classification_report,accuracy_score
from sklearn.metrics import plot_precision_recall_curve,precision_recall_curve,average_precision_score
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    X_train = pickle.load(open(f"{PICKLE_PATH}/X_train.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    y_train = pickle.load(open(f"{PICKLE_PATH}/y_train.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    clean_data = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/clean_data.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    modeling = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/modeling.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))

# Random Forest component

# example for fit_predict_score function
# this is data without oversampling 
## $xpr_param_component_name == model
## $xpr_param_component_type == pipeline_job
## $xpr_param_global_variables == ["X_train", "y_train"]
## $xpr_param_global_methods == ["clean_data", "modeling"]
def fit_predict_score(data_X,data_Y,name,clf):
   # X_train,X_test,y_train,y_test =train_test_split(data_X,data_Y,test_size=0.3,random_state=42)
    classifier=clf
    clf.fit(data_X, data_Y)
    predicted = clf.predict(X_val)  # prediction for validation 
    fitted_y=clf.predict(data_X)   # prediction for train 
    y_prob=clf.predict_proba(X_val)[:,1] # predict proba for validation 
    y_prob_train=clf.predict_proba(data_X)[:,1] 
    print("Processing...")
    
    confusion_matrix =  pd.crosstab(index=y_val, 
                                    columns=predicted.ravel(), 
                                    rownames=['Expected'], 
                                    colnames=['Predicted'])
    
    accuracy = np.round(accuracy_score(y_val , predicted),3)
    sns.heatmap(confusion_matrix, annot=True, square=False, fmt='', cbar=False)
    plt.title(name + ", Accuracy: " + str(accuracy), fontsize = 15)
    plt.show()
    
    brier=brier_score_loss(y_val, y_prob)
    average_precision = average_precision_score(y_val, y_prob)
    
    brier_train=brier_score_loss(data_Y, y_prob_train)
    disp = plot_precision_recall_curve(classifier, X_val, y_val)
    disp.ax_.set_title('2-class Precision-Recall curve: '
                   'AP={0:0.2f}'.format(average_precision))
    
    print("Model parameters ", "\n", classifier)
    
    print("Classification report for classifier in test ","\n",
               classification_report(y_val, predicted))
    print("Classification report for classifier in train ","\n",
              classification_report(data_Y, fitted_y))
    
    print("Brier Score for Train: ","\n",brier_train)
    print("Brier Score for Test: ","\n",brier)
    
    
    
    

fit_predict_score(X_train,y_train,name="Baseline Random Forest",
                  clf=RandomForestClassifier(random_state=42,max_features="sqrt",max_depth=10,
                                             criterion="entropy",class_weight="balanced"))

try:
    pickle.dump(marshal.dumps(clean_data.__code__), open(f"{PICKLE_PATH}/clean_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(modeling.__code__), open(f"{PICKLE_PATH}/modeling.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    pickle.dump(X_train, open(f"{PICKLE_PATH}/X_train.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(y_train, open(f"{PICKLE_PATH}/y_train.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

