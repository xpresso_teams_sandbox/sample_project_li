import os
import sys
import types
import pickle
import marshal
import numpy as np
import pandas as pd
import seaborn as sns
import xgboost as xgb
import matplotlib.pyplot as plt
from sklearn.metrics import brier_score_loss
from imblearn.over_sampling import SMOTE,SVMSMOTE
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix,classification_report,accuracy_score
from sklearn.metrics import plot_precision_recall_curve,precision_recall_curve,average_precision_score
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    X_train = pickle.load(open(f"{PICKLE_PATH}/X_train.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    y_train = pickle.load(open(f"{PICKLE_PATH}/y_train.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    clean_data = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/clean_data.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    modeling = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/modeling.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))

# xbgoost component
 
## $xpr_param_component_name == xgboost_model
## $xpr_param_component_type == pipeline_job
## $xpr_param_global_variables == ["X_train", "y_train"]
## $xpr_param_global_methods == ["clean_data", "modeling"]

D_val = xgb.DMatrix(X_val, label=y_val)

param = {
    'eta': 0.01, 
    'max_depth': 10, 
    'objective': 'binary:logistic',  
    'lambda': 0.2} 

steps = 20

def xgb_function(train_x,train_y,name):
    D_train = xgb.DMatrix(train_x, label=train_y)
    model_xgb = xgb.train(param, D_train,num_boost_round=30)
    # save model
    preds_xgb = model_xgb.predict(D_val)
    preds_train_xgb = model_xgb.predict(D_train)
    preds_xgb_class = np.where(preds_xgb >0.5, 1, 0)
    fitted_xgb=np.where(model_xgb.predict(D_train) >0.5, 1, 0)
    # best_preds
    confusion_xgb_matrix =  pd.crosstab(index=y_val, columns=preds_xgb_class.ravel(), 
                                    rownames=['Expected'], colnames=['Predicted'])
    sns.heatmap(confusion_xgb_matrix, annot=True, square=False, fmt='', cbar=False)
    accuracy_xgb = np.round(accuracy_score(y_val , preds_xgb_class),3)
    plt.title(name + ", Accuracy: " + str(accuracy_xgb), fontsize = 15)
    plt.show()
    print("Classification report for classifier %s:"
     % (classification_report(y_val, preds_xgb_class)))
    
    
    brier=brier_score_loss(y_val, preds_xgb)
    
    brier_train=brier_score_loss(train_y, preds_train_xgb)
    
    print("Classification report for classifier in test ","\n",
               classification_report(y_val, preds_xgb_class))
    print("Classification report for classifier in train ","\n",
              classification_report(train_y, fitted_xgb))
    
    print("Brier Score for test: ","\n",brier)
    print("Brier Score for train: ","\n",brier_train)
xgb_function(X_train,y_train,'baseline XGB')
# save model everytime 
# imbalance ratio in train 
# original_ratio=len(y_train[y_train==1])/len(y_train[y_train==0])
# val_ratio=len(y_val[y_val==1])/len(y_val[y_val==0])
# val_ratio
# original_ratio

try:
    pickle.dump(marshal.dumps(clean_data.__code__), open(f"{PICKLE_PATH}/clean_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(modeling.__code__), open(f"{PICKLE_PATH}/modeling.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    pickle.dump(X_train, open(f"{PICKLE_PATH}/X_train.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(y_train, open(f"{PICKLE_PATH}/y_train.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

