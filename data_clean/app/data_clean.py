import os
import sys
import types
import pickle
import marshal
import numpy as np
import pandas as pd
import seaborn as sns
import xgboost as xgb
import matplotlib.pyplot as plt
from sklearn.metrics import brier_score_loss
from imblearn.over_sampling import SMOTE,SVMSMOTE
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix,classification_report,accuracy_score
from sklearn.metrics import plot_precision_recall_curve,precision_recall_curve,average_precision_score
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    dat_X = pickle.load(open(f"{PICKLE_PATH}/dat_X.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    dat_target = pickle.load(open(f"{PICKLE_PATH}/dat_target.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    clean_data = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/clean_data.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name == data_clean
## $xpr_param_component_type == pipeline_job
## $xpr_param_global_variables == ["dat_X", "dat_target"]
## $xpr_param_global_methods == ["clean_data"]

# data feature engineering 
train = pd.read_csv('train.csv').iloc[:,1:]

X_train_split, X_val_split = train_test_split(train, test_size=0.2,
                                   stratify=train['target'], 
                                   random_state=1)

y_train = X_train_split['target']
X_train = X_train_split.drop(['target'], axis=1)

y_val = X_val_split['target']
X_val = X_val_split.drop(['target'], axis=1)
print('Train: shape X',X_train.shape,', shape Y',y_train.shape)
print('Test: shape X',X_val.shape,', shape Y',y_val.shape)

try:
    pickle.dump(marshal.dumps(clean_data.__code__), open(f"{PICKLE_PATH}/clean_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    pickle.dump(dat_X, open(f"{PICKLE_PATH}/dat_X.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(dat_target, open(f"{PICKLE_PATH}/dat_target.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

